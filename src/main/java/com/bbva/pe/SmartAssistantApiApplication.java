package com.bbva.pe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartAssistantApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartAssistantApiApplication.class, args);
	}

}
