package com.bbva.pe.dto;

public class CustomerDTO {

	protected String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	
}
