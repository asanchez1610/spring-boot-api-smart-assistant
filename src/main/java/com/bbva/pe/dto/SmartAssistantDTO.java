package com.bbva.pe.dto;

public class SmartAssistantDTO {

	protected String customerId;
	
	protected String cardNumber;
	
	protected String contractNumber;
	
	protected String accountNumber;
	
	protected String fcrType;
	
	protected String complaintId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getFcrType() {
		return fcrType;
	}

	public void setFcrType(String fcrType) {
		this.fcrType = fcrType;
	}

	public String getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(String complaintId) {
		this.complaintId = complaintId;
	}
	
}
