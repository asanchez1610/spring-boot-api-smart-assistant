package com.bbva.pe.dto;

public class SmartAssistantRequest {

	protected SmartAssistantDTO inputSmartAssistant;

	public SmartAssistantDTO getInputSmartAssistant() {
		return inputSmartAssistant;
	}

	public void setInputSmartAssistant(SmartAssistantDTO inputSmartAssistant) {
		this.inputSmartAssistant = inputSmartAssistant;
	}

}
