package com.bbva.pe.api;

import java.util.Map;

import com.bbva.pe.dto.OperationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.pe.api.service.SmartAssistantService;
import com.bbva.pe.dto.CustomerDTO;
import com.bbva.pe.dto.SmartAssistantRequest;

@RestController
public class ApiSmartAssistant {

	@Autowired
	private SmartAssistantService service;

	@GetMapping(value = "/")
	public String init() {
		return "Ok!";
	}

	@PostMapping(value = "/listOperation")
	public ResponseEntity<?> listOperation(@RequestBody CustomerDTO customerDTO) {
		Map<String, Object> response = service.listOperation(customerDTO);
		if (response == null) {
			return ResponseEntity.badRequest().body(null);
		}
		return ResponseEntity.ok().body(response);
	}
	
	@PostMapping(value = "/createOperation")
	public ResponseEntity<?> createOperation(@RequestBody SmartAssistantRequest	 smartAssistantRequest) {
		Map<String, Object> response = service.createOperation(smartAssistantRequest);
		if (response == null) {
			return ResponseEntity.badRequest().body(null);
		}
		return ResponseEntity.ok().body(response);
	}

	@PostMapping(value = "/getOperation")
	public ResponseEntity<?> getOperation(@RequestBody OperationDto operationDtoRequest) {
		Map<String, Object> response = service.getOperation(operationDtoRequest);
		if (response == null) {
			return ResponseEntity.badRequest().body(null);
		}
		return ResponseEntity.ok().body(response);
	}

}
