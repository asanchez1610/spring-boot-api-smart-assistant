package com.bbva.pe.api.service;

import java.io.IOException;
import java.util.Map;

import com.bbva.pe.dto.OperationDto;
import org.springframework.stereotype.Service;

import com.bbva.pe.dto.CustomerDTO;
import com.bbva.pe.dto.SmartAssistantRequest;
import com.bbva.pe.util.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

@Service
@SuppressWarnings({ "deprecation", "unchecked" })
public class SmartAssistantService {

	private OkHttpClient client;
	private Gson gson;

	public SmartAssistantService() {
		client = new OkHttpClient().newBuilder().build();
		gson = new Gson();
	}

	public Map<String, Object> listOperation(CustomerDTO customerDTO) {

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, gson.toJson(customerDTO));
		Request request = new Request.Builder().url(Constants.HOST.concat(Constants.LIST_OPERATION))
				.method("POST", body).addHeader("Accept", "application/json")
				.addHeader("Content-Type", "application/json").addHeader("header-name", "QP06")
				.addHeader("logical-transaction-code", "PREST002").addHeader("type-code", "15")
				.addHeader("subtype-code", "17").addHeader("version-code", "01").addHeader("country-code", "PE")
				.addHeader("entity-code", "0011").addHeader("branch-code", "0486").addHeader("workstation-code", "W963")
				.addHeader("operative-entity-code", "0011").addHeader("operative-branch-code", "0486")
				.addHeader("ip-address", "118.222.161.30").addHeader("channel-code", "01")
				.addHeader("environ-code", "01").addHeader("product-code", "0001").addHeader("language-code", "ES")
				.addHeader("user-code", "ZG13001").addHeader("operation-date", "20210105")
				.addHeader("operation-time", "103000").addHeader("currency-code", "PEN")
				.addHeader("secondary-currency-code", "604").addHeader("request-id", "PREST002-01-PE")
				.addHeader("service-id", "012345678901234").addHeader("client-identification-type", "L")
				.addHeader("client-document", "09313658").addHeader("contact-identifier", "29071967")
				.addHeader("authorization-version", "09").addHeader("authorization-code", "15172027")
				.addHeader("accounting-terminal", "001").addHeader("logical-terminal", "01")
				.addHeader("sequence-id", "20").addHeader("aap", "ether").addHeader("app", "ether-apx")
				.addHeader("origin", "Onboarding").addHeader("agent-user", "p028162")
				.addHeader("manager-user", "p028162").addHeader("mac", "E8-6A-64-F7-B007")
				.addHeader("remaining-time", "235959").build();
		try {
			ResponseBody responseBody = client.newCall(request).execute().body();
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, Object> mapEntity = objectMapper.readValue(responseBody.string(), Map.class);
			return mapEntity;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public Map<String, Object> createOperation(SmartAssistantRequest smartAssistantRequest) {
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, gson.toJson(smartAssistantRequest));
		Request request = new Request.Builder().url(Constants.HOST).method("POST", body)
				.addHeader("Accept", "application/json").addHeader("Content-Type", "application/json")
				.addHeader("logical-transaction-code", "PREST001").addHeader("version-code", "01")
				.addHeader("country-code", "PE").addHeader("header-name", "QP06").addHeader("type-code", "15")
				.addHeader("subtype-code", "17").addHeader("entity-code", "0011").addHeader("branch-code", "0486")
				.addHeader("workstation-code", "W963").addHeader("operative-entity-code", "0011")
				.addHeader("operative-branch-code", "0486").addHeader("ip-address", "118.222.161.30")
				.addHeader("channel-code", "01").addHeader("environ-code", "01").addHeader("product-code", "0001")
				.addHeader("language-code", "ES").addHeader("user-code", "ZG13001")
				.addHeader("operation-date", "20210105").addHeader("operation-time", "103000")
				.addHeader("currency-code", "PEN").addHeader("secondary-currency-code", "604")
				.addHeader("request-id", "PREST001-01-PEd").addHeader("service-id", "012345678901234")
				.addHeader("client-identification-type", "L").addHeader("client-document", "09313658")
				.addHeader("contact-identifier", "29071967").addHeader("authorization-version", "09")
				.addHeader("authorization-code", "15172027").addHeader("accounting-terminal", "001")
				.addHeader("logical-terminal", "01").addHeader("sequence-id", "20").addHeader("aap", "ether")
				.addHeader("app", "ether-apx").addHeader("origin", "Onboarding").addHeader("agent-user", "p028036")
				.addHeader("manager-user", "p028036").addHeader("mac", "E8-6A-64-F7-B007")
				.addHeader("remaining-time", "235959").build();
		try {
			ResponseBody responseBody = client.newCall(request).execute().body();
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, Object> mapEntity = objectMapper.readValue(responseBody.string(), Map.class);
			return mapEntity;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public Map<String, Object> getOperation(OperationDto operationDtoRequest) {
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, gson.toJson(operationDtoRequest));
		Request request = new Request.Builder().url(Constants.HOST.concat(Constants.GET_OPERATION)).method("POST", body)
				.addHeader("Accept", "application/json").addHeader("Content-Type", "application/json")
				.addHeader("country-code", "PE").addHeader("header-name", "QP06").addHeader("type-code", "15")
				.addHeader("subtype-code", "17").addHeader("entity-code", "0011").addHeader("branch-code", "0486")
				.addHeader("workstation-code", "W963").addHeader("operative-entity-code", "0011")
				.addHeader("operative-branch-code", "0486").addHeader("ip-address", "118.222.161.30")
				.addHeader("channel-code", "01").addHeader("environ-code", "01").addHeader("product-code", "0001")
				.addHeader("language-code", "ES").addHeader("user-code", "ZG13001")
				.addHeader("operation-date", "20210105").addHeader("operation-time", "103000")
				.addHeader("currency-code", "PEN").addHeader("secondary-currency-code", "604")
				.addHeader("client-identification-type", "L").addHeader("client-document", "09313658")
				.addHeader("contact-identifier", "29071967").addHeader("authorization-version", "09")
				.addHeader("authorization-code", "15172027").addHeader("accounting-terminal", "001")
				.addHeader("logical-terminal", "01").addHeader("sequence-id", "20").addHeader("aap", "ether")
				.addHeader("app", "ether-apx").addHeader("origin", "Onboarding").addHeader("agent-user", "p028036")
				.addHeader("manager-user", "p028036").addHeader("mac", "E8-6A-64-F7-B007")
				.addHeader("logical-transaction-code", "PREST006").addHeader("version-code", "01")
				.addHeader("request-id", "PREST006-01-PE").addHeader("service-id", "012345678901234")
				.addHeader("remaining-time", "235959").build();
		try {
			ResponseBody responseBody = client.newCall(request).execute().body();
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, Object> mapEntity = objectMapper.readValue(responseBody.string(), Map.class);
			return mapEntity;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
}
